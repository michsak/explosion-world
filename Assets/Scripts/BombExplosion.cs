﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombExplosion : MonoBehaviour
{
    private bool isPlanted = true;
    private float countdown = 2f;

    void Update()
    { 
        if (isPlanted)
        {
            Vector3 bombPosition = transform.position;
            isPlanted = false;
        }
            countdown -= Time.deltaTime;

        if (countdown <= 0f)
        {
            FindObjectOfType<DestroyMap>().Explode(transform.position);
            Destroy(gameObject);
        }
    }
}
