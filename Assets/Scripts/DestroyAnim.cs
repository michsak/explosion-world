﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAnim : MonoBehaviour
{       
    public void DestroyAnimation()
    {
        if (transform.parent.gameObject)
        {
            Destroy(transform.parent.gameObject);
        }
    }
}
