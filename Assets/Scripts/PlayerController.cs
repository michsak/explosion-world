﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//how invoke is working


public class PlayerController : MonoBehaviour
{
    private float speed = 7.5f;
    float horizontalMove = 0f;
    float verticalMove = 0f;
    public GameObject bomb;
#if UNITY_ANDROID && !UNITY_EDITOR
    float clicked = 0;
    float clicktime = 0;
    float clickdelay = 0.5f;
#endif
    [SerializeField] Joystick joystick;


    void FixedUpdate()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
            JoystickMove();
#else
        MoveUsingArrows();

        if (Input.GetButtonDown("Jump") && FindObjectOfType<CountPlayersHealthsAndBombs>().bombs > 0)
        {
            PlantingBomb();
        }
#endif
    }

    private void PlantingBomb()
    {
        Vector3 bombPosition = transform.position;
        if (Mathf.RoundToInt(bombPosition.x) % 2 == 0)
        {
            bombPosition.x += 1f;
            Instantiate(bomb, new Vector3(Mathf.RoundToInt(bombPosition.x), Mathf.RoundToInt(bombPosition.y), transform.position.z), Quaternion.identity);
            Invoke("RemoveBomb", 1f);
        }
        else if (Mathf.RoundToInt(bombPosition.y) % 2 == 0)
        {
            bombPosition.y += 1f;
            Instantiate(bomb, new Vector3(Mathf.RoundToInt(bombPosition.x), Mathf.RoundToInt(bombPosition.y), transform.position.z), Quaternion.identity);
            Invoke("RemoveBomb", 1f);
        }
        else
        {
            Instantiate(bomb, new Vector3(Mathf.RoundToInt(bombPosition.x), Mathf.RoundToInt(bombPosition.y), transform.position.z), Quaternion.identity);
            Invoke("RemoveBomb", 1f);
        }
    }

    private void MoveUsingArrows()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * speed * Time.deltaTime;
        verticalMove = Input.GetAxisRaw("Vertical") * speed * Time.deltaTime;
        transform.Translate(new Vector3(horizontalMove, verticalMove, 0f));
    }

    private void JoystickMove()
    {
        horizontalMove = joystick.Horizontal * speed * Time.deltaTime;
        verticalMove = joystick.Vertical * speed * Time.deltaTime;
        gameObject.transform.Translate(new Vector3(horizontalMove, verticalMove, 0f));
    }

    private void RemoveBomb()
    {
        FindObjectOfType<CountPlayersHealthsAndBombs>().bombs -= 1;
    }

#if UNITY_ANDROID && !UNITY_EDITOR          //To plant bomb doubleclick
    public void OnPointerClick(PointerEventData eventData)
    {
       clicked++;

        if (clicked == 1) { clicktime = Time.time; }
        if (clicked > 1 && Time.time - clicktime < clickdelay)
        {
            clicked = 0;
            clicktime = 0;
            PlantingBomb();
        }
        else if (clicked > 2 || Time.time - clicktime > 1) { clicked = 0; }
    }
#endif
}
