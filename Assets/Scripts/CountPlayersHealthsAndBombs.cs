﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountPlayersHealthsAndBombs : MonoBehaviour
{
    public int lives = 3;
    public int bombs = 20;
    [SerializeField] Text NumberOfBombs;
    [SerializeField] Text NumberOfLives;
    [SerializeField] GameObject EndGameCanvas;

    private void Start()
    {
        EndGameCanvas.SetActive(false);
    }

    private void Update()
    {
        NumberOfBombs.text = bombs.ToString();
        NumberOfLives.text = lives.ToString();

        if (lives <= 0)
        {
            Time.timeScale = 0;
            EndGameCanvas.SetActive(true);
        }
    }



}
