﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class DestroyMap : MonoBehaviour
{
    [SerializeField] Tilemap tilemap;
    [SerializeField] Tilemap tilemapStones;
    [SerializeField] Tilemap tilemapPaths;
    [SerializeField] Tilemap tilemapBreakable;
    [SerializeField] Tile wallTile;
    [SerializeField] Tile breakableTile;
    [SerializeField] Tile pathTile;
    [SerializeField] GameObject Anim;
    [SerializeField] GameObject player;


    public void Explode(Vector2 worldPos)
    {
        Vector3Int originCell = tilemap.WorldToCell(worldPos);
        ExplodeCell(originCell);
    }

    private void ExplodeCell(Vector3Int cell)
    {
        Vector3Int posRight_1 = cell + new Vector3Int(1, 0, 0);
        Vector3Int posRight_2 = cell + new Vector3Int(2, 0, 0);
        Vector3Int posLeft_1 = cell - new Vector3Int(1, 0, 0);
        Vector3Int posLeft_2 = cell - new Vector3Int(2, 0, 0);
        Vector3Int posUp_1 = cell + new Vector3Int(0, 1, 0);
        Vector3Int posUp_2 = cell + new Vector3Int(0, 2, 0);
        Vector3Int posDown_1 = cell - new Vector3Int(0, 1, 0);
        Vector3Int posDown_2 = cell - new Vector3Int(0, 2, 0);
        Vector3Int[] positions = new Vector3Int[] { posRight_1, posRight_2, posLeft_1, posLeft_2, posUp_1, posUp_2, posDown_1, posDown_2 };

        Tile tileRight_1 = tilemap.GetTile<Tile>(posRight_1);
        Tile tileRight_2 = tilemap.GetTile<Tile>(posRight_2);
        Tile tileLeft_1 = tilemap.GetTile<Tile>(posLeft_1);
        Tile tileLeft_2 = tilemap.GetTile<Tile>(posLeft_2);
        Tile tileUp_1 = tilemap.GetTile<Tile>(posUp_1);
        Tile tileUp_2 = tilemap.GetTile<Tile>(posUp_2);
        Tile tileDown_1 = tilemap.GetTile<Tile>(posDown_1);
        Tile tileDown_2 = tilemap.GetTile<Tile>(posDown_2);
        Tile[] allTiles = new Tile[] { tileRight_1, tileRight_2, tileLeft_1, tileLeft_2, tileUp_1, tileUp_2, tileDown_1, tileDown_2 };


        for (int j = 0; j<allTiles.Length; j++)
        {
            if (allTiles[j] == wallTile && j % 2 == 0) { j++; continue; }
            if (allTiles[j] == pathTile || allTiles[j] == breakableTile)
            {
                GameObject Animation = Instantiate(Anim, tilemap.CellToWorld(positions[j]), Quaternion.identity) as GameObject;
                if (Vector3.Distance(Animation.transform.position + new Vector3(1f, 1f, 0), player.transform.position) <  1f)
                {
                    FindObjectOfType<CountPlayersHealthsAndBombs>().lives -= 1;
                }
            }
        }
        

        for (int i = 0; i < allTiles.Length; i++)
        {
            if (allTiles[i] == wallTile && i%2 == 0) { i++; continue; }
            if (allTiles[i] == breakableTile)
            {
                tilemapBreakable.SetTile(positions[i], null);
                tilemap.SetTile(positions[i], pathTile);
                tilemapPaths.SetTile(positions[i], pathTile);
            }
        }
    }
}
